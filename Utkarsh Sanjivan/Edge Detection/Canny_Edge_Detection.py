import numpy as np
import cv2

img = cv2.imread('./Images/edgeDetection.jpg')
edge = cv2.Canny(img, 70, 280)
# sobel = cv2.Sobel(img, 3, 1, 1, 3, 1, 0, cv2.BORDER_DEAFULT)

cv2.imshow('Original Image', img)
# cv2.imshow('Edge Detected', edge)

cv2.waitKey(0)
cv2.destroyAllWindows()
