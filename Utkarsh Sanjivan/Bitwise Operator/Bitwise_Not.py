import numpy as np
import cv2

img1 = cv2.imread('./Images/ball1.jpeg',0)
# cv2.imshow('Image 1',img1)

ret, mask1 = cv2.threshold(img1,80, 255, cv2.THRESH_BINARY)
# cv2.imshow('Image BW',mask1)

b_not = cv2.bitwise_not(mask1)
cv2.imshow('NOT',b_not)


cv2.waitKey(0)
cv2.destroyAllWindows()
