import numpy as np
import cv2

img1 = cv2.imread('./Images/ball1.jpeg',0)
img2 = cv2.imread('./Images/ball2.jpg',0)
# cv2.imshow('Image 1',img1)
# cv2.imshow('Image 2',img2)

ret, mask1 = cv2.threshold(img1,80, 255, cv2.THRESH_BINARY)
ret, mask2 = cv2.threshold(img2,80, 255, cv2.THRESH_BINARY)
# cv2.imshow('Image BW 1',mask1)
# cv2.imshow('Image BW 2',mask2)

b_or = cv2.bitwise_or(mask1, mask2)
cv2.imshow('OR',b_or)


cv2.waitKey(0)
cv2.destroyAllWindows()
