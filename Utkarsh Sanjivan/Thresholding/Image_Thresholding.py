import numpy as np
import cv2

img = cv2.imread ('./Images/a.jpg',0)

retval, threshold = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY)

cv2.imshow('Original Image',img)
cv2.imshow('Thresholding',threshold)

cv2.waitKey(0)
cv2.destroyAllWindows
