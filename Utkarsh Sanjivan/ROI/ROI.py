import numpy as np
import cv2

image = cv2.imread('./Images/JOHAN-CRUYFF.jpg',1)
img = image.copy()
cropped_image=img[545:635,10:110]

cv2.imshow('Original Image',image)
cv2.imshow('Cropped Image',cropped_image)

cv2.waitKey(0)
cv2.destroyAllWindows()
