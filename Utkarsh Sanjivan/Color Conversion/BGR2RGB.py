import numpy as np
import cv2

img = cv2.imread('./Images/AndresIniesta.jpeg',1);
newImage = img.copy();

swapped_image=newImage[50:150, 50:150];

for row in range(0,99):
  for column in range(0,99):
      blue = swapped_image[row,column][0];
      green = swapped_image[row,column][1];
      red = swapped_image[row,column][2];
      swapped_image[row,column]=[red,green,blue];

cv2.imshow('image',img)
cv2.imshow("Swapped Image",swapped_image)
cv2.waitKey(0)
cv2.destroyAllWindows()
