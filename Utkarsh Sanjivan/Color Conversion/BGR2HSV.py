import cv2
import numpy as np

img = cv2.imread('./Images/color_detection.jpeg',1);
hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV);

cv2.imshow('Original Image',img);
cv2.imshow('HSV Image',hsv_img);

cv2.waitKey(0);
cv2.destroyAllWindows();
