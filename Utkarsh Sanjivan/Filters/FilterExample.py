import numpy as np
import cv2
# from matplotlib impom8rt pyplot as plt

img = cv2.imread('./Images/AndresIniesta.jpeg',1);
img1 = cv2.imread('./Images/AndresIniesta.jpeg',1);
img2 = cv2.imread('./Images/AndresIniesta.jpeg',1);
img3 = cv2.imread('./Images/AndresIniesta.jpeg',1);

avg_blur = cv2.blur(img, (3,3));
gaussian_blur = cv2.GaussianBlur(img1,(3,3),0);
gaussian_blur =  cv2.medianBlur(img2, 5);
bilateral_blur =  cv2.bilateralFilter(img3,9,75,75 );

cv2.imshow('Image',img);
cv2.imshow('Average Blur',avg_blur);
cv2.imshow('Gaussian Blur',gaussian_blur);
cv2.imshow('Median Blur',gaussian_blur);
cv2.imshow('Bilateral Blur',bilateral_blur);

# plt.subplot(141),plt.imshow(avg_blur),plt.title('Average Blur',);
# plt.subplot(142),plt.imshow(gaussian_blur),plt.title('Gaussian Blur');
# plt.subplot(143),plt.imshow(median_blur),plt.title('Median Blur');
# plt.subplot(144),plt.imshow(bilateral_blur),plt.title('Bilateral Blur');

cv2.waitKey(0);
cv2.destroyAllWindows();
