import numpy as np
import cv2

img1 = cv2.imread('./Images/gray1.png')
img2 = cv2.imread('./Images/gray2.png')

a = np.uint8(img1)
b = np.uint8(img2)

img_add = cv2.add(a,b)
img_o = a+b

cv2.imshow('cv2.add()',img_add)
cv2.imshow('+',img_o)

cv2.waitKey(0)
cv2.destroyAllWindows()
