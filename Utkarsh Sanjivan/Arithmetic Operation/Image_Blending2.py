import numpy as np
import cv2

img = cv2.imread('./Images/messi.jpg',1)
logo = cv2.imread('./Images/Logo Barcelona FC.png',1)
# cv2.imshow('Image',img)
# cv2.imshow('Logo',logo)

logo_u = logo[ 25:605, 270:920]
# cv2.imshow('Logo Extracted',logo_u)

logo_s = cv2.resize(logo_u,(100,100),interpolation=cv2.INTER_CUBIC)
# cv2.imshow('After Resize',logo_s)

roi = img[0:100,0:100]
img2gray = cv2.cvtColor(logo_s,cv2.COLOR_BGR2GRAY)
ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
mask_inv = cv2.bitwise_not(mask)
# cv2.imshow('ROI',roi)
# cv2.imshow('Gray',img2gray)
# cv2.imshow('Mask',mask)
# cv2.imshow('Mask Inverted',mask_inv)

img1_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
# cv2.imshow('After AND',img1_bg)

img2_fg = cv2.bitwise_and(logo_s,logo_s,mask = mask)
# cv2.imshow("ABC",img2_fg)

dst = cv2.add(img1_bg,img2_fg)
img[0:100, 0:100 ] = dst
# cv2.imshow('Final add',dst)
cv2.imshow('Final',img)

cv2.waitKey(0)
cv2.destroyAllWindows()
