import sys
import cv2
def main(argv):
    print("""
    Zoom In-Out demo
    ------------------
    * [i] -> Zoom [i]n
    * [o] -> Zoom [o]ut
    * [ESC] -> Close program
    """)

    src = cv2.imread('./Images/JOHAN-CRUYFF.jpg',1)

    while 1:
        rows, cols, _channels = src.shape

        cv2.imshow('Pyramids Demo', src)

        k = cv2.waitKey(0)
        if k == 27:
            break

        elif chr(k) == 'i':
            src = cv2.pyrUp(src, dstsize=(2 * cols, 2 * rows))
            print ('** Zoom In: Image x 2')

        elif chr(k) == 'o':
            src = cv2.pyrDown(src, dstsize=(cols / 2, rows / 2))
            print ('** Zoom Out: Image / 2')

    cv2.destroyAllWindows()
    return 0
if __name__ == "__main__":
    main(sys.argv[1:])
