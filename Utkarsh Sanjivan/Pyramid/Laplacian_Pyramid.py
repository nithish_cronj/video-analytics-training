import sys
import cv2
def main(argv):
    print("""
    Zoom In-Out demo
    ------------------
    * [i] -> Zoom [i]n
    * [o] -> Zoom [o]ut
    * [ESC] -> Close program
    """)

    src = cv2.imread('./Images/color_detection.jpeg',1)
    print("src",src.shape)

    while 1:
        rows, cols, _channels = src.shape

        cv2.imshow('Gaussian Pyramids Demo', src)

        k = cv2.waitKey(0)
        if k == 27:
                break

        elif chr(k) == 'i':
            src0 = cv2.pyrUp(src, dstsize=(2 * cols, 2 * rows))
            src1 = cv2.pyrDown(src0)
            srcd = cv2.subtract(src, src1)
            src=src0
            cv2.imshow('Laplace Pyramid',srcd)
            print ('** Zoom In: Image x 2')

        elif chr(k) == 'o':
            src0 = cv2.pyrDown(src, dstsize=(cols / 2, rows / 2))
            src1 = cv2.pyrUp(src0)
            srcd = cv2.subtract(src, src1)
            src=src0
            cv2.imshow('Laplace Pyramid',srcd)
            print ('** Zoom Out: Image / 2')

    cv2.destroyAllWindows()
    return 0
if __name__ == "__main__":
    main(sys.argv[1:])
