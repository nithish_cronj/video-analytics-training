import numpy as np
import cv2

image = cv2.imread('./Images/AffineTransformationPic.png',1)
img = image.copy()
rows,cols,ch = image.shape

pts1 = np.float32([[0,0],[348,40],[163,469]])
pts2 = np.float32([[0,0],[cols,0],[0,rows]])
M = cv2.getAffineTransform(pts1,pts2)
dst = cv2.warpAffine(img,M,(cols,rows))

cv2.imshow('Original Image',image)
cv2.imshow('Affine Transformation',dst)

cv2.waitKey(0)
cv2.destroyAllWindows()
