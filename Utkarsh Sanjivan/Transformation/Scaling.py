import numpy as np;
import cv2;

img = cv2.imread('./Images/AndresIniesta.jpeg',1);

res = cv2.resize(img,None, fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC);

cv2.imshow('Original Image',img);
cv2.imshow('Sacled Image with coodrinate',res);

cv2.waitKey(0);
cv2.destroyAllWindows();
