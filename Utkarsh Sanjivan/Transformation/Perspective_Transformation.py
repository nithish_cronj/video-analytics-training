import numpy as np
import cv2

image = cv2.imread('./Images/2.jpg',1)
img = image.copy()
rows,cols,ch = image.shape

pts1 = np.float32([[326,141], [520,188], [548,519],[711,465]])
pts2 = np.float32([[0,0],[200,0],[0,200],[200,200]])
M = cv2.getPerspectiveTransform(pts1,pts2)
dst = cv2.warpPerspective(img,M,(200,200))

cv2.imshow('Original Image',image)
cv2.imshow('Perspective Transformation',dst)

cv2.waitKey(0)
cv2.destroyAllWindows()
