import numpy as np
import cv2

image = cv2.imread('./Images/AndresIniesta.jpeg',1)
img = image.copy()

rows,cols,ch = image.shape
M = cv2.getRotationMatrix2D((cols/2,rows/2),90,1)
dst = cv2.warpAffine(img,M,(cols,rows))

cv2.imshow('Original Image',image)
cv2.imshow('Rotated Image',dst)

cv2.waitKey(0)
cv2.destroyAllWindows()
