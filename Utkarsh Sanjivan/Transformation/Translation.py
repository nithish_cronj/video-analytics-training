import numpy as np
import cv2

image = cv2.imread('./Images/AndresIniesta.jpeg',1)
img = image.copy()

rows,cols,ch = image.shape
M = np.float32([[1,0,50],[0,1,100]])
dst = cv2.warpAffine(img,M,(cols,rows))

cv2.imshow('Original Image',image)
cv2.imshow('Translated Image',dst)

cv2.waitKey(0)
cv2.destroyAllWindows()
