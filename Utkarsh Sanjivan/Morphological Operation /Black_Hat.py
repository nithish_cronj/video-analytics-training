import numpy as np
import cv2

img = cv2.imread('./Images/leaf.jpg',0)
ret_val, binary_img = cv2.threshold(img,140,255,cv2.THRESH_BINARY)
binary_inv = cv2.bitwise_not(binary_img)
kernel = np.ones((5,5),np.uint8)
blackhat = cv2.morphologyEx(binary_inv,cv2.MORPH_BLACKHAT,kernel)

# cv2.imshow('Original Image', img)
cv2.imshow('Binary Form', binary_inv)
cv2.imshow('Black Hat Operation',blackhat)

cv2.waitKey(0)
cv2.destroyAllWindows()
