import numpy as np
import cv2

img = cv2.imread('./Images/white-pic.png',1);
lin = cv2.line(img,(10,10),(100,100),(0,0,255),5);
bb = cv2.rectangle(img,(10,150),(40,200),(155,80,99),4);
cir = cv2.circle(img,(150,150),50,(50,250,10),5);
ell = cv2.ellipse(img,(150,50),(50,20),60,0,360,[100, 100, 255],5);


cv2.imshow('Original Image',img);

cv2.waitKey(0);
cv2.destroyAllWindows();
