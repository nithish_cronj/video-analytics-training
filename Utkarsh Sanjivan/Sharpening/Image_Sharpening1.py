import numpy as np
import cv2

img = cv2.imread('./Images/JOHAN-CRUYFF.jpg')
cv2.imshow('Image',img)

frame = cv2.GaussianBlur(img,(0,0),3)
img_s = cv2.addWeighted(frame,-0.5,img,1.5,0)
cv2.imshow('Sharpen Image',img_s)

cv2.waitKey(0)
cv2.destroyAllWindows()
